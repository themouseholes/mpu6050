#include "I2Cdev.h"
#include "MPU6050.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
	#include "Wire.h"
#endif

MPU6050 accelgyro;

int16_t ax,ay,az;
int16_t gx,gy,gz;

#define OUTPUT_READABLE_ACCELGYRO

void setup() 
{
	#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
		Wire.begin();
	#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
		Fastwire::setup(400,true);
	#endif

    // initialize serial communication
    // (38400 chosen because it works as well at 8MHz as it does at 16MHz, but
    // it's really up to you depending on your project)
	 Serial.begin(38400);

   // initialize device
  Serial.println("Initializing I2C devices...");
  accelgyro.initialize();

  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");


}

void loop()
{
    // read raw accel/gyro measurements from device
    //accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

    accelgyro.getRotation(&gx, &gy, &gz);

    turnRight();
    turnLeft();
}
void turnRight()
{
    Serial.println("Begin 90 degree right turn");
    int leftWheel = 0;
    int rightWheel = 0;

    while(gz!=90){
        leftWheel += 1;
        rightWheel -= 1;
        //print current rotation about z
        Serial.print(gz); Serial.print("\t");

        //print wheel orientation
        Serial.print(leftWheel); Serial.print("\t");Serial.println(rightWheel);

        //update orientation
        gz = accelgyro.getRotationZ();
    }

        if(gz == 90)
            Serial.println("90 degree right turn made!");
}

void turnLeft()
{
    Serial.println("Begin 90 degree left turn");
    int leftWheel = 0;
    int rightWheel = 0;

    while(gz!=-90){
        leftWheel -= 1;
        rightWheel += 1;
        //print current rotation about z
        Serial.print(gz); Serial.print("\t");

        //print wheel orientation
        Serial.print(leftWheel); Serial.print("\t");Serial.println(rightWheel);

        //update orientation
        gz = accelgyro.getRotationZ();
    }

    if(gz == -90)
        Serial.println("90 degree left turn made!");
    
}
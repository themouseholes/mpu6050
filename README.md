# README #

### What is this repository for? ###

Quick test and implementation of MPU6050 with arduino for micromouse application
Version 1.0

### How do I get set up? ###
connect MPU6050 as shown in tutorial
https://diyhacking.com/arduino-mpu-6050-imu-sensor-tutorial/

install libraries:
I2Cdev: http://diyhacking.com/projects/I2Cdev.zip
MPU6050: http://diyhacking.com/projects/MPU6050.zip

### Who do I talk to? ###
Jugo